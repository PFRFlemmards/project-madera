# Livrable 2 : « Modélisation et analyse »

[Rapport]

Vous devrez proposer une architecture d’application, la définition de l’ergonomie et de l’Interface Homme-Machine.

Vous devrez également intégrer la description des modules de l’application et des traitements à réaliser pour répondre aux besoins exprimés.

Votre documentation du livrable 2 devra obligatoirement contenir au minimum les éléments suivants :
- 01 - La modélisation UML et merise avec l’architecture de la base de données supportant l’application
- 02 - Les préalables à la réalisation des séquences et les dépendances externes
- 03 - Les contrôles de saisies/données
- 04 - La définition de l’ergonomie et de l’IHM (Mock-Up),
- 05 - La description des modules de l’application et des traitements de l’information associés,
- 06 - La définition de l’environnement de développement de l’application,
- 07 - La méthode de développement qui vous parait la mieux adaptée à la typologie et à la taille du projet
- 08 - L’argumentation des choix d’outils de développement et de base de données associées (critères, coûts, ressources, ...)
