# Note de cadrage - Projet MADERA
RIL/DevOps 2018 • S.Gosselin - R.Heller - D.Leborgne - G.Peden - A.Minot

## Sujet
### La société
Situé sur le territoire français, le groupe Madera a été créé en 1990. L'entreprise est spécialisée dans la construction en bois pour les particuliers et collectivités.

Ils possèdent plusieurs sites à Lille, Dax, Annecy, Brest et Macon. Le siège social est situé à Lille. Dans la majorité, les différents sites se compose d'un site de production, un entrepôt de stockage des matières première et des produits finis ainsi que des bureaux et un magasin de vente à la clientèle.

Le groupe a développé son activité en proposant à ses clients la construction de maison modulaire et écologiques en bois. La structure de chaque maison modulaire est un assemblage dit de "structure porteuse bois".

En 2013 le groupe a réalisé un chiffre d'affaire de 200 millions d'euro et vendu 146 maison modulaires (15% du CA réalisé)

### Dénomination sociale de la société MADERA
- **Adresse :** 59000, Lille
- **Numéro de téléphone :** 03 20 55 66 77
- **Dirigeant :** Alexandre CROUS
- **N° de SIRET :** 81763497500010
- **Siège social :** 59000, Lille
- **Date d'immatriculation du RCS :** 01/01/1990
- **Tranche d'effectif :** 185 salariés
- **Capital :** 1 000 000,00 €

### Le projet
Le client souhaite lancer le développement d’un applicatif spécifique pour les commerciaux de la société Madera permettant la création de devis de maisons modulaires. Certains services seront impactés au niveau de l’exploitation de l’applicatif, par la réalisation du projet ou par la bonne réussite du projet.

## Objectifs du client
L’objectif principal du client est de dynamiser son chiffre d’affaire et de gagner des parts de marché sur ses concurrents européens à travers le lancement du produit.

L’applicatif existant est aujourd’hui vieillissant, avec des mises à jour mettant parfois plusieurs semaines à venir, et n’est plus compatible avec les nouveaux outils numériques des commerciaux.

L’application - qui doit être en lien avec le traitement des fournisseurs - doit permettre de créer des devis de maison modulaire en anticipant les commandes de fourniture dès l'acceptation du devis.

Le produit devra augmenter la réactivité de la chaine de production grâce à des outils simples et rapides pour animer les commerciaux, tout en gérant une volumétrie de commande supérieure à celle que l’entreprise traite actuellement.

## Critères de terminaison du projet
### Le projet doit
- pouvoir géré trois fois plus commandes sur une année grâce à une gestion supérieure de la volumétrie,
- permettre d’anticiper les commandes de fourniture dès l’acceptation d’un devis,
- augmenter la réactivité de la chaine de production.

## Périmètre du projet
### Le projet doit permettre de
- Créer un devis prévisionnel décrivant les caractéristiques de la maison modulaire souhaitée par le client.
- Intégrer les composants des modules utilisés par les commerciaux pour réaliser un projet de maison modulaire comme défini par le bureau d’étude.
- Accélérer les délais : les commandes seront lancées vers les fournisseurs dès l’acceptation du devis.
- Prendre en compte les potentielles pertes de connexion Internet (mode offline).
- Produire un retour sur investissement sur une période de 5 ans.
- Pouvoir être utilisé sur tablette par les commerciaux, en présence du client.

### Le projet ne doit pas
- Excéder le budget de 110 000€.
- Faire appel à de la sous-traitance.
- Intégrer une gestion de stock
- Gérer le paiement des maisons

## Calendirer du déroulement du projet
- Livrable 0 : 08/10/2018
- Livrable 1 : 15/03/2019
- Livrable 2 : 05/07:2019
- Livrable 3 : 22/11/2019
- Livrable 4 : 05/06/2010

#### Livrable 0 : Prise de connaissance
Consiste à créé une note de cadrage afin de :

- prendre connaissance de l'activité de l'entreprise cliente
- décrire succinctement le projet souhaité par le client
- prendre connaissance des objectifs du client
- définir les critères de terminaison du projet
- définir le prérimètre du projet

#### Livrable 1 : Lancement du projet
Consiste à établire les objectifs et la statégie à mettre en oeuvre pour la réalisation du projet.

Ce livrable devra contenir :

- Reformulation du besion
- Les résultats souhaités (exclusions/inclusions, contraintes/hypothèses, limites/tolérances)
- Schéma de décomposition du projet (PBS)
- Identification du commanditaire et partie prenantes
- Structure de l'équipe projet (rôles de chacun)
- Planning prévisionnel et rentébilisation du projet
- Analyse des risques
- Indicateurs de suivie et réussite de projet
- Structure du projet (WBS)

#### Livrable 2 : « Modélisation et analyse »
Consiste à proposer une architecture d’application, la définition de l’ergonomie et de l’Interface Homme, Machine. Il doit également intégrer la description des modules de l’application et des traitements à réaliser pour répondre aux besoins exprimés.

Le livrable 2 devra contenir au minimum les éléments suivants :

- La modélisation UML et merise avec l’architecture de la base de données supportant l’application Les préalables à la réalisation des séquences et les dépendances externes
- Les contrôles de saisies/données
- La définition de l’ergonomie et de l’IHM (Mock-Up),
- La description des modules de l’application et des traitements de l’information associés,
- La définition de l’environnement de développement de l’application,
- La méthode de développement qui vous parait la mieux adaptée à la typologie et à la taille du projet
- L’argumentation des choix d’outils de développement et de base de données associées (critères, coûts, ressources, ...)

#### Livrable 3 : « Développement du prototype »
Consiste à développer un prototype du module « Conception de devis », établir la budgétisation prévisionnelle du projet et mettre en place une politique de sécurisation du projet.

Le prototype livré devra comprendre une interface mobile.

La documentation du livrable 3 devra contenir les éléments suivants :

- Les tâches associées à ce livrable (préciser entrants/extrants, indicateur(s) de suivi, ressource(s), amplitude, durée, cout)
- Les scénarios de tests
- Le budget prévisionnel du projet comprenant l’investissement, les charges, les coûts humains
internes et externes,
- Une politique de sécurisation de l’application en rédigeant un plan d’assurance qualité autour des
processus de développement et de maintenance de l’application.
- Un plan de gestion des risques de l’application

#### Livrable 4 : « Rapport finale »
Le rapport final est une synthèse de l’ensemble des rapports. Il devra faire ressortir les solutions techniques proposées ainsi que les arguments retenus. Le rapport devra contenir :

- Le plan de management du projet,
- Les choix technologiques et la stratégie retenus pour l’architecture de l’application,
- Les solutions techniques proposées,
- La politique de sécurisation de l’application,
- Un PRA/PCA : un plan de sauvegarde des données, un plan de restauration, traçabilité des
données...
- Le descriptif des outils à mettre en place pour assurer la continuité de service.
- Un plan de déploiement et le suivi de la mise en place de l’outil (choix d’un outil de suivi des
remarques et demandes d’évolution par exemple),
- Un plan de communication,
- Un plan de formation des utilisateurs,
- Le retour d’expérience (REX).
