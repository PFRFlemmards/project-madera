# Livrable 4 : « Rapport et soutenance finale »

[Rapport + soutenance]

## Le rapport

Le rapport final est une synthèse de l'ensemble des rapports qui ne devra pas excéder 60 pages (hors
annexes).

Le contenu du rapport reste libre et à l'appréciation de chaque participant mais il devra au
minimum faire ressortir les solutions techniques proposées ainsi que les arguments retenus.

Le rapport devra contenir au minimum :
- Le plan de management du projet,
- Les choix technologiques et la stratégie retenus pour l'architecture de l'application,
- Les solutions techniques proposées,
- La politique de sécurisation de l'application,
- Un PRA/PCA : un plan de sauvegarde des données, un plan de restauration, traçabilité des données...
- Le descriptif des outils à mettre en place pour assurer la continuité de service.
- Un plan de déploiement et le suivi de la mise en place de l'outil (choix d’un outil de suivi des remarques et demandes d’évolution par exemple),
- Un plan de communication,
- Un plan de formation des utilisateurs,
- Le retour d’expérience (REX).


## La soutenance

La soutenance finale de chaque groupe est à réaliser au cours d'une séance de présentation d'une durée de 30 minutes maximum.

Vous devrez présenter le projet et défendre la solution proposée comme si vous deviez la présenter à votre direction (avantages, inconvénients, risques, détails techniques, coûts, délais, planifications, garanties ...) et à ses collègues techniques (principes techniques, technologies mises en œuvre).

A la fin de la présentation, le jury pourra poser différentes questions en rapport avec la présentation mais également sur le rapport dans sa globalité. Chaque participant devra y répondre avec le plus d'objectivité possible.

Cette évaluation portera sur l'ensemble des axes du projet fil rouge (proposition technique, méthodologie d’ingénierie logiciel mise en œuvre, pilotage du projet, présentation écrite et orale du projet) et intégrera les points forts et points à travailler des solutions proposées.
