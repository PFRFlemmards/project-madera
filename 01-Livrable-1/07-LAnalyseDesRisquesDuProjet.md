# Analyse des Risques du Projet Madera

## Typologie du projet informatique et nature des risques associés
Le projet Madera peut être caractérisé comme ayant un objectif d'Efficience à destination des Commerciaux par le remplacement et d’outillage d’un processus existant en vue d’accroître la productivité.

### Objectifs

Les risques connus liés à cette typologie de projet informatique en matière d'objectifs sont les suivants :
* Appropriation insuffisante du Système d’Informations par les utilisateurs
* Sous-estimation globale du projet,
* minimisation des coûts
* Dérive technologique

### Cibles
La mise en place du projet Madera va impacter le support et l'organisation de l'entreprise.

Le projet visant l'amélioration du fonctionnement interne de l’entreprise, les risques suivants sont à prendre en compte :
* Non-remise en cause de l’existant
* Sous-estimation des travaux
* Modification de l’environnement
* Rejet par les opérationnels

De plus, étant le projet Madera étant un outil Transversal, il touche l’ensemble de l’organisation, ce qui entraîne les risques suivants :
* Définition insuffisante de l’objectif
* Structuration inadéquate du projet
* Sous-estimation de l’utilisation

### Type de Solution
Les Projets de conception et de développement d’un logiciel spécifique sont connus pour porter certains risques :
* Insuffisance du cahier des charges (besoins et solutions)
* Manque de compétences ou de pérennité de l'équipe

Le projet Madera venant remplacer une application existante, nous pouvons ajouter les risques liés à la maintenance évolutive, et ceux liés à l'intégration de progiciels applicatifs :
* Absence ou indisponibilité des ressources (humaines et/ou documentaires)
* Mauvaise analyse d’impacts des modifications envisagées
* Pas de remise en cause de l’existant
* Gestion du changement déficiente
* Sous-estimation des travaux de migration et d’interfaçage

## Risques externes
liés à l’environnement de l’entreprise, : son activité́, son marché, ses concurrents, les réglementations, ...

### Risques techniques
* Evolution

### Risques politiques
* Entreprise
* Lobbying
* Social
* Contestation

### Risques liés au client, au marché
* Financement
* Évolution des besoins
* Concurrence
* Utilisation

### Risques juridiques
* Sécurité́
* Environnement
* Fiscalité

## Risques internes
liés à l’organisation de l’entreprise, son management, ses processus, ses systèmes d’information, ...

### Risques humains
* Organisation
* Animation
* Communication interne
* Décision
* ...

### Risques organisationnels
* Planification
* Suivi
* Documentation
* Version
* Budget

### Risques technologiques
* Ergonomie
* Sécurité́
* Compétence
* Disponibilité́
* Adéquation

### Risques contractuels
* Exigences
* Cahier des charges

## Risques de pilotage
liés aux informations nécessaires pour prendre les bonnes décisions :
* Reporting financier
* Tableaux de bord
* ...



| ID  | Risques                | Facteur de risques | Gravité | Probabilité | Criticité |
| :-: | :--------------------: | :----------------: | :-----: | :---------: | :-------: |
| 1   | Evolution              |                    |         |             |           |
| 2   | Entreprise             |                    |         |             |           |
| 3   | Lobbying               |                    |         |             |           |
| 4   | Social                 |                    |         |             |           |
| 5   | Contestation           |                    |         |             |           |
| 6   | Financement            |                    |         |             |           |
| 7   | Évolution des besoins  |                    |         |             |           |
| 8   | Concurrence            |                    |         |             |           |
| 9   | Utilisation            |                    |         |             |           |
| 10  | Sécurité́               |                    |         |             |           |
| 11  | Environnement          |                    |         |             |           |
| 12  | Fiscalité              |                    |         |             |           |
| 13  | Organisation           |                    |         |             |           |
| 14  | Animation              |                    |         |             |           |
| 15  | Communication interne  |                    |         |             |           |
| 16  | Décision               |                    |         |             |           |
| 17  | Planification          |                    |         |             |           |
| 18  | Suivi                  |                    |         |             |           |
| 19  | Documentation          |                    |         |             |           |
| 20  | Version                |                    |         |             |           |
| 21  | Budget                 |                    |         |             |           |
| 22  | Ergonomie              |                    |         |             |           |
| 23  | Sécurité́               |                    |         |             |           |
| 24  | Compétence             |                    |         |             |           |
| 25  | Disponibilité́          |                    |         |             |           |
| 26  | Adéquation             |                    |         |             |           |
| 27  | Exigences              |                    |         |             |           |
| 28  | Cahier des charges     |                    |         |             |           |
| 29  | Reporting financier    |                    |         |             |           |
| 30  | Tableaux de bord       |                    |         |             |           |
