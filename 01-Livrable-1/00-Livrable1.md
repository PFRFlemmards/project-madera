# Livrable 1 : Lancement du projet
[Rapport + soutenance]

Vous devrez présenter pour cette soutenance les objectifs et la stratégie que vous comptez mettre en
œuvre pour la réalisation de ce projet.

Votre documentation du livrable 1 devra obligatoirement contenir au minimum les éléments suivants :

## Analyse Fonctionnelle des services attendus
- [x] 01 - [La reformulation du besoin](01-LaReformulationDuBesoin.md)
- [x] 02 - [Les résultats souhaités (les exclusions et la portée du projet, les contraintes et hypothèses, les limites et tolérances)](02-LesResultatssouhaites.md)
- [ ] 03 - Un schéma général de décomposition du projet (PBS)
- [x] 04 - [L'identification du commanditaire et les parties prenantes connues](04-PartiesPrenantes.md)
- [ ] 05 - Structure de l'équipe de gestion de projet et la description des rôles (inclure les fiches de poste)
- [x] 06 - [Planning prévisionnel du projet et l'approche de rentabilisation](06-PlanningPrevisionnelDuProjetEtLApprocheDeRentabilisation.md)
- [ ] 07 - [L'analyse des risques du projet](07-LAnalyseDesRisquesDuProjet.md)
- [ ] 08 - [Les indicateurs de pilotage/suivi de projet et les indicateurs de réussite du projet](08-Indicateurs.md)
- [x] 09 - [La structure de découpage du projet (WBS)](09-LaStructureDeDecoupageDuProjet-WBS.md)

## Gestion du système documentaire
- [ ] 10 - [Mode de stockage des documents et méthode de classement](10-Mode-de-stockage-des-documents-et-methode-de-classement.md)
- [ ] 11 - Identification des versions
- [ ] 12 - Enregistrement de la configuration

