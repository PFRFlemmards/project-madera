# Les résultats souhaités
## Critères de terminaison du projet
### Le projet doit
* pouvoir géré trois fois plus commandes sur une année grâce à une gestion supérieure de la volumétrie,
* permettre d'anticiper les commandes de fourniture dès l'acceptation d'un devis,
* augmenter la réactivité de la chaine de production.

## Périmètre du projet
### Le projet doit permettre de
* Créer un devis prévisionnel décrivant les caractéristiques de la maison modulaire souhaitée par le client.
* Intégrer les composants des modules utilisés par les commerciaux pour réaliser un projet de maison modulaire comme défini par le bureau d'étude.
* Accélérer les délais : les commandes seront lancées vers les fournisseurs dès l'acceptation du devis.
* Prendre en compte les potentielles pertes de connexion Internet (mode offline).
* Produire un retour sur investissement sur une période de 5 ans.
* Pouvoir être utilisé sur tablette par les commerciaux, en présence du client.

### Le projet ne doit pas
* Excéder le budget de 110 000€.
* Faire appel à de la sous-traitance.
* Intégrer une gestion de stock
* Gérer le paiement des maisons