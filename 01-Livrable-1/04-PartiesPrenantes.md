# Parties Prenantes

## Identification des Parties Prenantes

### Interne
- Chef de Projet
  - Steven
- Equipe projet
  - Chef de Projet
  - 4 Responsables en Ingéniérie Logicielle
- Services Support
  - Responsable Formation
  - Service Compta
  - Technicien HelpDesk
  - Directeur RH
- Comanditaire
  - Directeur Financier
  - Comité de Pilotage (cf. P19)
    - PDG
    - DG
    - DAF
    - Chef de Projet
- Direction
  - PDG
  - Directeur Général
- Experts
  - Intervenants CESI
  - Chargé de mission Juridique
  - Responsable R&D
  - Responsable Achats
  - Responsable Qualité
  - Responsable Informatique
  - Responsable de Bureau d'Étude
  - Responsable Commercial BU Maison Modulaire

### Externes
- Client
  - Commercial
  - BE
  - Compta 
  - Client (si accès)
- Fournisseurs
  - BE
  - Commercial
- Organismes publics
  - CNIL (RGPD)
  - Batîment de france -> validation Devis par BE pour vérification juridique avant envoi client


## Matrice de Pouvoir
### Interne

1. 	Chef de Projet
2. 	Equipe projet
3. 	Service support
4. 	Comanditaire
5. 	Direction
6. 	Intervenants CESI
7. 	Chargé de mission Juridique
8. 	Responsable R&D
9. 	Responsable Achats
10. Responsable Qualité
11. Responsable Informatique
12. Responsable de Bureau d'Étude
13. Responsable Commercial BU Maison Modulaire

### Externes
14\. Client   
15\. Fournisseurs  
16\. Organismes publics

![MatriceDePouvoir](04-MatriceDePouvoir.png)

## Matrice d'Engagement

| Parties Prenantes     | Incertaines | Receptive |  Neutre | Favorable | Meneur |
| :-------------------- | :---------: | :-------: | :-----: | :-------: | :----: |
|                                     **Interne**                                |
| 1  Chef de Projet     |             |           |         |           |   AS   |
| 2  Equipe projet      |             |           |         |           |   AS   |
| 3  Service support    |             |           |         |    AS     |        |
| 4  Comanditaire       |             |           |         |           |   AS   |
| 5  Direction          |             |           |         |    AS     |        |
| 6  Intervenants CESI  |             |   AS      |         |           |        |
| 7  Chargé Juridique   |             |   AS      |         |           |        |
| 8  Rsp R&D            |             |   AS      |         |           |        |
| 9  Rsp Achats         |             |   AS      |         |           |        |
| 10 Rsp Qualité        |             |   AS      |         |           |        |
| 11 Rsp Informatique   |             |           |         |           |  AS    |
| 12 Rsp Bureau Étude   |             |           |         |           |  AS    |
| 13 Rsp Comm BU MM     |             |           |         |    AS     |        |
|                                     **Externes**                               |
| 14 Client             |             |           |         |           |  AS    |
| 15 Fournisseurs       |             |   AS      |         |           |        |
| 16 Organismes publics |             |   AS      |         |           |        |

A : Souhaité - A : Actuel
