# Planning prévisionnel du projet et l'approche de rentabilisation

## Livrable 1 : Lancement du projet

| Date | Description |
|--|--|
| 17/01/2019 | Analyse fonctionnelle des services attendus |
| 13/02/2019 | Gestion documentaire & Analyse fonctionnelle des services attendus|
| **15/03/2019** | **Rendu du livrable 1**

---

## Livrable 2 : « Modélisation et analyse »

| Date | Description |
|--|--|
| 16/05/2019 | Architecture d'application, ergonomie et IHM  |
| 13/06/2019 | Description des modules et des traitements de l'application (Visio) |
| **05/07/2019** | **Rendu du livrable 2**

---

## Livrable 3 : « Développement du prototype »

| Date | Description |
|--|--|
| 29/08/2019 | Liste des tâches associées au livrables et scénarios de tests |
| 26/09/2019 | Budget prévisionnel du projet |
| 24/10/2019 | Plan d'assurance qualité de gestion des risques de l'application |
| **22/11/2019** | **Rendu du livrable 3**

---

## Livrable 4 : « Rapport finale »

| Date | Description |
|--|--|
| 16/01/2020 | Plan du rapport |
| 12/03/2020 | Contenu du rapport |
| 16/04/2020 | Correction du rapport |
| **05/06/2020** | **Rendu du livrable 4** / Support de soutenance
