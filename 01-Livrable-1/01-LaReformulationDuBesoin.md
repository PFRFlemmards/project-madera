# Reformulation du besoin

## Présentation
Situé sur le territoire français, le groupe Madera a été créé en 1990. L'entreprise est spécialisée dans la construction en bois pour les particuliers et collectivités.

Possédant plusieurs sites à Dax, Annecy, Brest et Macon, son siège social est situé à Lille. Généralement, les différents sites se composent d'un site de production, un entrepôt de stockage des matières premières et des produits finis ainsi que des bureaux et un magasin de vente à la clientèle.

Le groupe a développé son activité en proposant à ses clients la construction de maisons modulaires et écologiques en bois. La structure de chaque maison modulaire est un assemblage dit de "structure porteuse bois".
En 2013, le groupe a réalisé un chiffre d'affaire de 200 millions d'euro et vendu 146 maisons modulaires (15% du CA réalisé).

Le client souhaite lancer le développement d'un applicatif spécifique pour les commerciaux de la société Madera, permettant la création de devis de maisons modulaires. Certains services seront impactés au niveau de l'exploitation de l'applicatif, par la réalisation du projet ou par la réussite du projet.

Un [document](../00-Projet_Madera.pdf) nous a été remis afin de mieux appréhender le projet. Ce document contient les informations jugées nécessaires par le client afin que nous en prenions connaissance. Il contient, entre autres, une expression du contexte et du besoin, un organigramme.

Nous allons ici redéfinir le besoin du client afin d'affiner les objectifs de ce projet.

## Redéfinition
Dans le cadre du projet Madera, nous nous concentrerons sur des maisons modulaires en bois pouvant être composées de : un rez de chaussée, un seul étage, un garage, une terrasse et un porche d'entrée. Le tout devra être assemblé dans les règles de l'art et les normes en vigeur.

Le but de l'application décrite dans le document fournit par le client est avant tout de promouvoir la nouvelle gamme "Maison Modulaire en Bois" de l'entreprise Madera. Cette promotion est prévue avec une augmentation du nombre de commandes, l'objectif du client étant de multiplier par trois le nombre actuel de commandes.

Afin de répondre à cette augmentation du nombre de commandes, la solution devra permettre la conception et l'édition des devis.

Chaque client est reconnu par l'application comme étant un "projet"; chaque projet peut contenir plusieurs plans de maisons modulaire, et donc plusieurs devis. En fonction de l'avancement du projet, un statut est défini pour permettre au client de suivre son avancé.

Chaque plan est modifiable et est enregistré automatiquement dès la fin de l'édition. Ceux-ci doivent par ailleurs être conservés, qu'ils soient validés ou refusés.
L'application doit pouvoir être utilisable sans connexion internet et hors du réseau de l'entreprise.

### La conception du produit

Le commercial, avec le client, dispose d'un choix de composants défini par le bureau d'étude. L'assemblage de ces composants en modules permettra au bureau d'étude de créer le plan de la maison modulaire.
Dans un soucis d'ergonomie et de gain de temps, les composants pourront être triés par catégories et gammes de produits.

Le client défini la composition de la maison en choisissant un modèle d'espace d'habitation, vierge et/ou modifiable.
Les murs intérieurs pourront être définis à condition qu'ils soient rattaché à un mur extérieur existant.
Par ailleurs, Le toit est géré comme un élément à part entière.

Par la suite, les ouvertures sont positionnées parmi un choix de composant : portes, fenêtres, baies vitrées, etc.

Une fois ces étapes effectuées, le commercial affectera différents modules au pièces précédemment définies. Le client aura finalement la possibilité de déterminer la composition et l'aspect de chacun des composant des modules.

### L'édition du devis
Le devis prévisionnel comprendra la listes de chaque modules, leurs composants et ses divers accessoires avec leurs prix unitaire et le nombre de ceux-ci. Il indiquera le prix hors taxes (HT) et toutes taxes comprises (TTC) du projet.

Ce devis prévisionnel sera remis au client et au bureau d'étude.

Finalement, une fois le devis accepté, et si besoin mis à jour et parallèlement aux plans d'exécution réalisés par le bureau d'étude, la procédure de facturation et de commande de matériaux est lancée.

