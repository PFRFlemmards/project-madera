# Mode de stockage des documents et méthode de classement
## Mode de stockage
_On décrit ici le mode stockage des documents du projet._

Suite à des problématiques de connexion internet pour certain des membres, nous avons essayé différents type d'outils collaboratif et synchrone/asynchrone.
1. Nous avons commencé par utiliser le logiciel de travail collaboratif Microsoft Teams. Nous avons dû l'abandonner car il n'était pas assez stable.
2. Dans un deuxième temps nous avons envisagé d'utiliser la suite Google Docs, seulement il est enclin à de fortes latence sur le système informatique de l'établissement de Brest du CESI.
3. Notre choix s'est porté sur l'utilisation d'un dépôt Git via la plateforme GitHub. Elle répond à nos attentes grâce à un fonctionnement collaboratif et asynchrone permettant de palier au problème de connexion internet.
**(PS - Steven: ne pas oublier de Commiter avant de Pousser !)**

GitHub nous permet de déposer n'importe quel type de document (.doc, .pdf, .md, etc...) et de les partager entre tout les membres de l'équipe projet.
De plus, l'outil intègre un suivi des modification du dépôt nous permettant de revenir en arrière en cas d'erreur.

## Méthode de classement
_On décrit ici la convention de nommage des documents qui définis notre méthode de classement._

Notre méthode de classement est fondée sur les différents livrables décrit dans le [document](../00-Projet_Madera.pdf) remis en début de projet.
  Au niveau de la racine, 5 dossiers sont créés, portant le nom de chaque livrable et précédés d'un numéro d'identification (exemple: 00-Livrable-0, 01-Livrable-1).
  Un cinquième dossier nommé "05-GestionDeProjet" contient tous les documents relatif à la gestion de projet.

Chaque livrable est composé de différents éléments.
  Sous le dossier du livrable correspondant, un fichier texte d'index portant le numéro d'identification "00" et le nom du livrable, contient ces différents éléments listés, numérotés (numéro d'identification) et pouvant être cochés une fois l'élément validé par tous (exemple: [01-Livrable-1.md](01-Livrable-1.md)).
  Pour chaque élément, un fichier portant son numéro d'identification et son libellé est créé.
  Ce fichier à pour rôle de répondre à la demande de l'élément à traiter.
  Chaque document "ressource" correspondant à un élément d'un livrable doit être précédé du numéro d'identification correspondant à l'élément à traité.
  Ainsi nous pouvons avoir plusieurs fichier correspondant à un élément.

Ci-dessous un exemple de l'arborescence de classement :

````txt
.
├── 00-Livrable-0
│   └── Livrable0.pdf
├── 00-Projet_Madera.pdf
├── 01-Livrable-1
│   ├── 00-Livrable1.md
│   ├── 01-BeteACorne.png
│   ├── 01-BeteACorne.xml
│   ├── 01-LaReformulationDuBesoin.md
│   ├── 01-Pieuvre.png
│   ├── 01-Pieuvre.xml
│   ├── 02-LesResultatssouhaites.md
│   ├── 04-MatriceDePouvoir.png
│   ├── 04-MatriceDePouvoir.xml
│   ├── 04-PartiesPrenantes.md
│   └── 10-ModeDestockageDesDocumentsEtMethodeDeClassement.md
├── 02-Livrable-2
│   └── 00-Livrable2.md
├── 03-Livrable-3
│   └── 00-Livrable3.md
├── 04-Livrable-4
│   └── 00-Livrable4.md
├── 05-GestionDeProjet
│   ├── Quesions.md
├── README.md
└── _config.yml
````
