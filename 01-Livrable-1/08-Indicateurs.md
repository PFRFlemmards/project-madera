# Les indicateurs de pilotage/suivi de projet et les indicateurs de réussite du projet

## Les indicateurs de pilotage/suivi de projet

- Taches réalisées par rapport aux tâches planifiées
- Utilisation des ressources
- Écart entre nombre d’heures travaillées et planifiées
- Mesure de satisfaction au travail du personnel
- Jalons

## Les indicateurs de réussite du projet 

- La qualité des services offerts
- Le taux de satisfaction client
- L’impact positif réel des actions du projet sur le marché cible
- Le retour sur investissement (ROI)
