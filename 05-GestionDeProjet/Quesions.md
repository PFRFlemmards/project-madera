
## Questions 17/01/19

- Identification des Parties Prenantes : Qui dans le service Compta ?
 - Le Service Compta (dans son ensemble)
- BE en équipe (agile) ou en expert ?
 - Au choix, en fonction de la place qu'on veut lui donner.
 - Le BE est dans les Clients de l'application.


## Questions 07/01/19
- A qui est envoyé le Devis une fois qu'il a été accepté par le client et transformé en facture ?
 - Au commercial ?
 - Au bureau d'étude ?
 - Au chef d'atelier ?
