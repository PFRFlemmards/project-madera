# Livrable 3 : « Développement du prototype »

[Rapport + présentation du prototype]

Vous devrez développer un prototype du module « Conception de devis », établir la budgétisation prévisionnelle du projet et mettre en place une politique de sécurisation du projet.
Le prototype livré devra comprendre une interface mobile.
Votre documentation du livrable 3 devra obligatoirement contenir au minimum les éléments suivants :

- Les tâches associées à ce livrable (préciser entrants/extrants, indicateur(s) de suivi, ressource(s), amplitude, durée, cout)
- Les scénarios de tests
- Le budget prévisionnel du projet comprenant l'investissement, les charges, les coûts humains internes et externes,
- Une politique de sécurisation de l'application en rédigeant un plan d’assurance qualité autour des processus de développement et de maintenance de l’application.
- Un plan de gestion des risques de l'application
